import React, { Component } from 'react';
import { StyleSheet, View } from "react-native";
import { Button } from 'react-native-elements'
import { Scene, Router, ActionConst, Stack, Modal, Tabs } from 'react-native-router-flux';

// Splash components
import Splash from '../components/Splash/Splash';

// Authentication Scenes
import Welcome from '../modules/auth/scenes/welcome';
import Register from '../modules/auth/scenes/register';
import CompleteProfile from '../modules/auth/scenes/completeProfile';
import Login from '../modules/auth/scenes/login';
import ForgotPassword from '../modules/auth/scenes/forgotPassword';
import Home from '../modules/home/scenes/home';
import Hamburger from 'react-native-hamburger';


// Import Store, actions
import store from '../redux/store'
import { checkLoginStatus } from "../modules/auth/actions";

import { fontFamily, normalize } from "../styles/theme"


export default class extends Component {
    constructor() {
        super();
        this.state = {
            isReady: false,
            isLoggedIn: false
        }
    }

    componentDidMount() {
        let _this = this;
        store.dispatch(checkLoginStatus((isLoggedIn) => {
            _this.setState({isReady: true, isLoggedIn});
        }));
    }

    render() {
        if (!this.state.isReady)
            return <Splash/>

        return(
            <Router>
                <Scene key="root"
                    navigationBarStyle={styles.navBar}
                    hideNavBar
                    titleStyle={styles.title}

                    backButtonTintColor={"rgba(0, 0, 0, 0.3)"}>
                    <Stack key="Auth" initial={!this.state.isLoggenIn} >
                        <Scene key="Welcome" component={Welcome} title="Welcome" initial={true}/>
                        <Scene key="Register" component={Register} title="Register" back/>
                        <Scene key="CompleteProfile" component={CompleteProfile} title="Select Username" back={false}/>
                        <Scene key="Login" component={Login} title="Login" back/>
                        <Scene key="ForgotPassword" component={ForgotPassword} title="ForgotPassword"/>
                    </Stack>
                    <Stack key="Main" initial={this.state.isLoggenIn} >
                        <Scene key="Home" component={Home} title="Home" initial={true} type={ActionConst.REPLACE} renderRightButton={Hamburger}/>
                    </Stack>
                </Scene>
            </Router>
        );
    }
}

const styles = StyleSheet.create({
    navBar:{
        backgroundColor:"#fff",
        borderBottomWidth:0
    },

    title:{
        fontSize: normalize(16),
        lineHeight: normalize(19),
        fontFamily: fontFamily.bold,
        color: "rgba(0,0,0,.84)"
    }
});
