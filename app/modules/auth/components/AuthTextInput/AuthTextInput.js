import React, {Component} from 'react';
import PropTypes from 'prop-types'

import { View } from 'react-native';

import FormInput from 'react-native-elements/src/form/FormInput'
import FormLabel from 'react-native-elements/src/form/FormLabel'
import FormValidationMessage from 'react-native-elements/src/form/FormValidationMessage'

import { isEmpty } from '../../utils/validate'
import styles from "./styles"

class AuthTextInput extends Component {
  render() {
    const { showLabel, placeholder, autoFocus, onChangeText, secureTextEntry } = this.props;

    return (
      <View style={styles.container}>
          {
              (showLabel) &&
              <FormLabel>{this.props.label}</FormLabel>
          }
          <FormInput
              autoCapitalize='none'
              clearButtonMode='while-editing'
              underlineColorAndroid={"#fff"}
              placeholder={placeholder}
              autoFocus={autoFocus}
              onChangeText={onChangeText}
              secureTextEntry={secureTextEntry}
              inputStyle={styles.inputContainer}
              value={this.props.value}
          />
          {
              (!isEmpty(this.props.error)) &&
              <FormValidationMessage>
                  {this.props.error}
              </FormValidationMessage>
          }
      </View>
    );
  }
}

AuthTextInput.propTypes = {
    label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    placeholder: PropTypes.string,
    autoFocus: PropTypes.bool,
    onChangeText: PropTypes.func.isRequired,
    secureTextEntry: PropTypes.bool,
    value: PropTypes.string,
    error: PropTypes.string,
}

AuthTextInput.defaultProps = {
    autoFocus: false,
    secureTextEntry: false
}

export default AuthTextInput;
