import { FACEBOOK_APP_ID } from "../../config/constants";

export const NAME = 'auth';
export const FACEBOOK_APPLICATION_ID = FACEBOOK_APP_ID;
