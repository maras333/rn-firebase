
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';

import { Button, SocialIcon, Divider, Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux';
import { Facebook } from 'expo';

import {actions as auth, constants as c } from "../../index"
const { signInWithFacebook } = auth;

import styles from "./styles"
import AuthContainer from "../../components/AuthContainer"

class Welcome extends Component {
  constructor() {
    super();
    this.state = {}

    this.onSuccess = this.onSuccess.bind(this);
    this.onError = this.onError.bind(this);
    this.onSignInWithFacebook = this.onSignInWithFacebook.bind(this);
  }

  async onSignInWithFacebook() {
      const options = { permissions: ['public_profile', 'email'] };
      const { type, token } = await Facebook.logInWithReadPermissionsAsync(c.FACEBOOK_APPLICATION_ID, options);

      if(type === 'success') {
          this.props.signInWithFacebook(token, this.onSuccess, this.onError)
      }
  }

  onSuccess({ exists, user }) {
      if (exists) Actions.Main()
      else Actions.CompleteProfile({ user })
  }

  onError(error) {
      alert(error.message)
  }

  render() {
    return (
      <AuthContainer>
          <View style={styles.container}>
              <View style={styles.topContainer}>
                  <Image style={styles.image} source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}/>
                  <Text style={styles.title}>TEST APP</Text>
              </View>
              <View style={styles.bottomContainer}>
                  <View style={[styles.buttonContainer]}>
                      <SocialIcon
                          raised
                          button
                          type='facebook'
                          title="SIGN UP WITH FACEBOOK"
                          iconSize={19}
                          style={[styles.containerView, styles.socialButton]}
                          fontStyle={styles.buttonText}
                          onPress={this.onSignInWithFacebook}
                      />
                      <View style={styles.orContainer}>
                          <Divider style={styles.divider}/>
                          <Text style={styles.orText}>
                              Or
                          </Text>
                      </View>
                      <Button
                          raised
                          text="SIGN UP WITH E-MAIL"
                          borderRadius={10}
                          containerViewStyle={[styles.containerView]}
                          buttonStyle={[styles.button]}
                          textStyle={styles.buttonText}
                          onPress={Actions.Register}
                      />
                  </View>
                  <View style={styles.bottom}>
                      <TouchableOpacity onPress={Actions.Login}>
                          <Text style={styles.bottomText}>
                              Already have an account?
                          </Text>
                          <Text style={styles.signInText}>
                              Sign in
                          </Text>
                      </TouchableOpacity>
                  </View>
              </View>
          </View>
      </AuthContainer>
    );
  }
}

export default connect(null, { signInWithFacebook })(Welcome);
