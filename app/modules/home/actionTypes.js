// export const HOME_SAVED_MOVIES_LIST = 'home/HOME_SAVED_MOVIES_LIST';
// from TMDb
export const FETCHING = 'home/FETCHING';
export const CONFIGURATION = 'home/CONFIGURATION';
export const FETCHED_LIST = 'home/FETCHED_LIST';
export const FETCHED_DETAILS = 'home/FETCHED_DETAILS';
// from DataBase
export const SAVE_MOVIE = 'home/SAVE_MOVIE';
export const GET_SAVED_MOVIES_LIST = 'home/GET_SAVED_MOVIES_LIST';
export const GET_SAVED_MOVIE_DETAILS = 'home/GET_SAVED_MOVIE_DETAILS';
