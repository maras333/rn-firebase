import * as t from './actionTypes';
import * as api from './api';
import { constants as c } from './index';
import firebase from "../../config/firebase";

import { AsyncStorage } from 'react-native';


// save movie to database
export function saveMovie(data, successCB, errorCB) {
    return (dispatch) => {
        api.saveMovie(data.currentUser, data.movie, (success, data, error) => {
            // if(success) {
            //     dispatch({type: t.SAVE_MOVIE, data: data});
            //     successCB(data);
            // } else if(error) {
            //     dispatch({type: t.SAVE_MOVIE, data: error});
            //     errorCB(error);
            // }
        });
    }
}

// get configuration of TMDb
export function getConfig(successCB, errorCB) {
    return (dispatch) => {
        let urlConfig = `https://api.themoviedb.org/3/configuration?api_key=${c.TMDB_API_KEY}`;
        api.fetchApi(urlConfig, (success, data, error) => {
            if(success) {
                dispatch({type: t.CONFIGURATION, data: data});
                successCB(data);
            } else if(error) {
                dispatch({type: t.CONFIGURATION, data: error});
                errorCB(error);
            }
        });
    }
}

// fetching list of matching movies
export function fetchMovie(query, successCB, errorCB) {
    return (dispatch) => {
        let movieID = 157336 // set initital load movie - Interstellar
        let urlSearch = `https://api.themoviedb.org/3/search/movie?query=${query}?&api_key=${c.TMDB_API_KEY}`;
        dispatch({type: t.FETCHING, data: true});
        api.fetchApi(urlSearch, (success, data, error) => {
            dispatch({type: t.FETCHING, data: false});
            if(success) {
                dispatch({type: t.FETCHED_LIST, data: data});
                successCB(data);
            } else if(error) {
                dispatch({type: t.FETCHED_LIST, data: error});
                errorCB(error);
            }
        });
    }
}

// fetching data about given movie with corresponding ID
export function fetchMovieDetails(movieID, successCB, errorCB) {
    return (dispatch) => {
        let urlSearchDetails = `https://api.themoviedb.org/3/movie/${movieID}?&api_key=${c.TMDB_API_KEY}`;
        dispatch({type: t.FETCHING, data: true});
        api.fetchApi(urlSearchDetails, (success, data, error) => {
            dispatch({type: t.FETCHING, data: false});
            if(success) {
                dispatch({type: t.FETCHED_DETAILS, data: data});
                successCB(data);
            } else if(error) {
                dispatch({type: t.FETCHED_DETAILS, data: error});
                errorCB(error);
            }
        });
    }
}


// TODO to integrate this action
export function getSavedMovies(userId, successCB, errorCB) {
  return (dispatch) => {
    // TODO fetching saved movies from firebase
    api.getSavedMovies(userId, (success, data, error) => {
      if(success) {
        dispatch({type: t.GET_SAVED_MOVIES_LIST, data: movies});
        successCB(movies);
      } else if (error) {
        dispatch({type: t.GET_SAVED_MOVIES_LIST, data: error});
        errorCB(error);
      }
    });
  }
}

// TODO to integrate this action
export function getMovieDetails(movieId, successCB, errorCB) {
  return (dispatch) => {
    // TODO fetching movie details from firebase
    api.getMovieDetails(movieId, (success, data, error) => {
      if(success) {
        dispatch({type: t.GET_SAVED_MOVIE_DETAILS, data: movie});
        successCB(movie);
      } else if (error) {
        dispatch({type: t.GET_SAVED_MOVIE_DETAILS, data: error});
        errorCB(error);
      }
    });
  }
}
