import firebase from "../../config/firebase";

// fetching data from TMDb api
export function fetchApi(url, callback) {
  fetch(url)
    .then((res) => {
        return res.json();
    })
    .then((data) => {
        console.log(data);
        callback(true, data, null);
    })
    .catch((err) => callback(false, null, error));
}

// save movie to DB
export function saveMovie(currentUser, movie, callback) {
    console.log("Call to api: ", "curUser: ", currentUser, "movie: ", movie);
// database.ref('users').child(user.uid).update({ ...user })
//   fetch(url)
//     .then((res) => {
//         return res.json();
//     })
//     .then((data) => {
//         callback(true, data, null);
//     })
//     .catch((err) => callback(false, null, error));
}

// get movies from DB
export function getSavedMovies(userId, callback) {
  // fetch(url)
  //   .then((res) => {
  //       return res.json();
  //   })
  //   .then((data) => {
  //       callback(true, data, null);
  //   })
  //   .catch((err) => callback(false, null, error));
}

// get movie details from DB
export function getMovieDetails(movieId, callback) {
  // fetch(url)
  //   .then((res) => {
  //       return res.json();
  //   })
  //   .then((data) => {
  //       callback(true, data, null);
  //   })
  //   .catch((err) => callback(false, null, error));
}
