
import React, { Component } from 'react';
import PropTypes from 'prop-types'

import { View, Text } from 'react-native';
import { Button } from 'react-native-elements'

import { validateSearch, isEmpty } from '../../utils/validate'

import styles from "./styles"

import TextInput from "../TextInput"

class SearchForm extends Component {
  constructor(props) {
    super(props);

    const fields = props.fields;
    const error = props.error;
    const state = {};

    for(let i=0; i<fields.length; i++) {
      let field = fields[i];
      let {key, type, value} = field;
      state[key] = {type: type, value: value};
    }

    state["error"] = error;
    this.timeout = null;
    this.state = state;

    this.onSubmit = this.onSubmit.bind(this);
    this.extractData = this.extractData.bind(this);
  }

  onSubmit() {
    // const data = this.state;
    // const result = validateSearch(data);
    // if(!result.success) this.setState({error: result.error});
    // else this.props.onSearchMovie(this.extractData(data));
  }

  extractData(data) {
    const retData = {};

    Object.keys(data).forEach((key) => {
      if(key === "keywords") {
        let { value } = data[key];
        retData[key] = value;
      }
    });

    return retData['keywords'];
  }

  onChange(key, text) {
      clearTimeout(this.timeout);
      const state = this.state;
      state[key]['value'] = text;
      this.setState(state);

      let data = this.state;
      this.timeout = setTimeout(() => { this.props.onSearchMovie(this.extractData(data)) }, 1000);
      // this.props.onSearchMovie(this.extractData(data));
  }

  onFetchDetails(key, movieId, title) {
      const state = this.state;
      // state[key]['value'] = text;
      // this.setState(state);
      //
      state[key]['value'] = title;
      this.setState(state);

      console.log(movieId);
      this.props.onSearchDetails(movieId)
      // this.timeout = setTimeout(() => { this.props.onSearchDetails(movieID) }, 1000);
      // this.props.onSearchMovie(this.extractData(data));
  }

  componentWillUnmount() {
      clearTimeout(this.timeout);
  }

  render() {
    const { fields, showLabel, buttonTitle, films } = this.props;

    return (
      <View style={styles.wrapper}>
          {
              (!isEmpty(this.state.error['general'])) &&
              <Text style={styles.errorText}>{this.state.error['general']}</Text>
          }
          {
              fields.map((data, idx) => {
                  let {key, label, placeholder, autoFocus, secureTextEntry} = data;
                  return (
                      <TextInput key={key}
                          label={label}
                          showLabel={showLabel}
                          placeholder={placeholder}
                          autoFocus={autoFocus}
                          onChangeText={(text) => this.onChange(key, text)}
                          onFetchDetails={(movieId, title) => this.onFetchDetails(key, movieId, title)}
                          secureTextEntry={secureTextEntry}
                          films={films}
                          value={this.state[key]['value']}
                          error={this.state.error[key]}/>
                  )
              })
          }
          {/* <Button
              raised
              text={buttonTitle}
              borderRadius={4}
              containerViewStyle={styles.buttonContainer}
              buttonStyle={styles.button}
              textStyle={styles.buttonText}
              onPress={this.onSubmit}
          /> */}
      </View>
    )
  }
}

SearchForm.propTypes = {
    fields: PropTypes.arrayOf(PropTypes.object),
    showLabel: PropTypes.bool,
    buttonTitle: PropTypes.string,
    onSearchMovie: PropTypes.func.isRequired,
    onSearchDetails: PropTypes.func.isRequired,
    error: PropTypes.object,
    films: PropTypes.object
}

export default SearchForm;
