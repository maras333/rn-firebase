import React, {Component} from 'react';
import PropTypes from 'prop-types'

import { View, TouchableOpacity, Text } from 'react-native';
import Autocomplete from 'react-native-autocomplete-input';

import FormInput from 'react-native-elements/src/form/FormInput'
import FormLabel from 'react-native-elements/src/form/FormLabel'
import FormValidationMessage from 'react-native-elements/src/form/FormValidationMessage'

import { isEmpty } from '../../utils/validate'
import styles from "./styles"

class TextInput extends Component {
      constructor(props) {
          super(props);
          this.state = {
              query: '',
              hideSuggestion: true
          }
          this.onChangeText = this.onChangeText.bind(this);
      }

      onChangeText(text) {
          this.props.onChangeText(text);
          this.setState({hideSuggestion: false})
      }

      render() {
        // console.log('text-- ', this.props.value);
        // console.log('sugestionBox-- ', this.state.hideSuggestion);
        const { films, showLabel, placeholder, autoFocus, onChangeText, onFetchDetails } = this.props;
        return (
          <View style={styles.container}>
              {
                  (showLabel) &&
                  <FormLabel>{this.props.label}</FormLabel>
              }
              <View style={styles.autocompleteContainer}>
                  <Autocomplete
                      hideResults={this.state.hideSuggestion}
                      autoCapitalize="none"
                      autoCorrect={false}
                      data={films ? films['results'] : []}
                      defaultValue={this.props.value}
                      inputContainerStyle={styles.inputContainer}
                      onChangeText={this.onChangeText}
                      placeholder="Enter Keywords"
                      renderItem={({ id, title, vote_average }) => (
                          <TouchableOpacity onPress={() => {onFetchDetails(id, title); this.setState({query: id, hideSuggestion: true})}}>
                              <Text style={styles.itemText}>
                                  {title} ({vote_average})
                              </Text>
                          </TouchableOpacity>
                      )}
                  />
              </View>


              {
                  (!isEmpty(this.props.error)) &&
                  <FormValidationMessage>
                      {this.props.error}
                  </FormValidationMessage>
              }
          </View>
        );
      }
}

TextInput.propTypes = {
    label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    placeholder: PropTypes.string,
    autoFocus: PropTypes.bool,
    onChangeText: PropTypes.func.isRequired,
    onFetchDetails: PropTypes.func.isRequired,
    value: PropTypes.string,
    films: PropTypes.object,
    error: PropTypes.string,
}

TextInput.defaultProps = {
    autoFocus: false,
}

export default TextInput;
