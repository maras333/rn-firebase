import { StyleSheet } from 'react-native';

import { theme } from "../../index"
const { windowWidth, fontSize, normalize } = theme;

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
    paddingTop: 25,
    flex: 1,
  },

  inputContainer: {
    width: windowWidth - 40,
    height: normalize(65),
    borderBottomColor: "#A5A7A9"
  },

  autocompleteContainer: {
    flex: 1,
    // left: 0,
    justifyContent: 'center',
    // position: 'absolute',
    // right: 0,
    // bottom: 0,
    // zIndex: 1
  },

  itemText: {
    fontSize: fontSize.regular + 2,
    margin: 2,
    width: windowWidth - 40
  },
});

export default styles;
