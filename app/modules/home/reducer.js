import { AsyncStorage } from 'react-native';

import * as t from './actionTypes';

let initialState = { isFetching: false, data: null, configData: null };

const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case t.CONFIGURATION:
      const configuration = action.data;

      state = Object.assign({}, state, { configData: configuration });
      return state;
    case t.FETCHING:
      const fetching = action.data;

      state = Object.assign({}, state, { isFetching: fetching });
      return state;

    case t.FETCHED_LIST:
      const moviesList = action.data;

      state = Object.assign({}, state, { data: moviesList });
      return state;

    case t.FETCHED_DETAILS:
      const fetchedMoviesDetails = action.data;

      state = Object.assign({}, state, { data: fetchedMoviesDetails });
      return state;

    case t.SAVE_MOVIE:
      const movie = action.data;
      state = Object.assign({}, state, { data: movie });
      return state;

    case t.GET_SAVED_MOVIES_LIST:
      const movies = action.data;
      state = Object.assign({}, state, { data: movies });
      return state;

    case t.GET_SAVED_MOVIE_DETAILS:
      const movieDetails = action.data;
      state = Object.assign({}, state, { data: movieDetails });
      return state;

    default:
      return state;
  }
}

export default homeReducer;
