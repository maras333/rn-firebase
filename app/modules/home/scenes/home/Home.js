
import React, { Component } from 'react';
var { View, ScrollView, StyleSheet, Alert, Image } = require('react-native');
import defaultImage from '../../../../assets/images/default_img.jpg';

import { Button, Input, Card, Text } from 'react-native-elements'
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'

import styles from "./styles"


import SearchForm from "../../components/SearchForm"

import { actions as auth, theme } from "../../../auth/index"
const { signOut } = auth;
const { color } = theme;
import { actions as homeActions } from "../../index"
const { getConfig, fetchMovie, fetchMovieDetails, saveMovie } = homeActions;


const fields = [
    {
        key: 'keywords',
        label: "Keywords",
        placeholder: "Search",
        autoFocus: false,
        secureTextEntry: false,
        value: "",
        type: "text"
    }
];

const error = {
    general: ""
}


class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error,
            films: {},
            details: {},
            config: {},
            configErr: {}
        }
        // get config
        this.onSuccessGetConfig = this.onSuccessGetConfig.bind(this);
        this.onErrorGetConfig = this.onErrorGetConfig.bind(this);
        // log out
        this.onSignOut = this.onSignOut.bind(this);
        this.onSuccess = this.onSuccess.bind(this);
        this.onError = this.onError.bind(this);
        // search movie
        this.onSearchMovie = this.onSearchMovie.bind(this);
        this.onSuccessSearch = this.onSuccessSearch.bind(this);
        this.onErrorSearch = this.onErrorSearch.bind(this);
        // get details
        this.onSearchDetails = this.onSearchDetails.bind(this);
        this.onSuccessSearchDetails = this.onSuccessSearchDetails.bind(this);
        this.onErrorSearchDetails = this.onErrorSearchDetails.bind(this);
        // save
        this.onSave = this.onSave.bind(this);
        this.onSuccessSave = this.onSuccessSave.bind(this);
        this.onErrorSave = this.onErrorSave.bind(this);
        console.log(props.homeState);
        console.log(props);

    }

    componentWillMount() {
        console.log('here');
        this.props.getConfig(this.onSuccessGetConfig, this.onErrorGetConfig);
    }

    onSuccessGetConfig(data) {
         // Alert.alert('Movie info: ', JSON.stringify(data));
         this.setState({ config: data })
    }

    onErrorGetConfig(error) {

        if(error.hasOwnProperty("message")) {
            errObj['general'] = error.message;
        } else {
            let keys = Object.keys(error);
            keys.map((key, idx) => {
                errObj[key] = error[key];
            });
        }
        this.setState({configErr: errObj});
    }

    onSearchMovie(data) {
        this.setState({error: error}); // clear out errors
        this.props.fetchMovie(data, this.onSuccessSearch, this.onErrorSearch)
    }

    onSuccessSearch(data) {
         // Alert.alert('Movie info: ', JSON.stringify(data));
         console.log('films-- ', data);
         this.setState({ films: data })
    }

    onErrorSearch(error) {
        let errObj = this.state.error;

        if(error.hasOwnProperty("message")) {
            errObj['general'] = error.message;
        } else {
            let keys = Object.keys(error);
            keys.map((key, idx) => {
                errObj[key] = error[key];
            });
        }
        this.setState({error: errObj});
    }

    onSearchDetails(data) {
        this.setState({error: error}); // clear out errors
        this.props.fetchMovieDetails(data, this.onSuccessSearchDetails, this.onErrorSearchDetails)
    }

    onSuccessSearchDetails(details) {
         // Alert.alert('Movie info: ', JSON.stringify(data));
         console.log('details-- ', details);
         this.setState({ details })
    }

    onErrorSearchDetails(error) {
        let errObj = this.state.error;
        console.log(error);
        if(error.hasOwnProperty("message")) {
            errObj['general'] = error.message;
        } else {
            let keys = Object.keys(error);
            keys.map((key, idx) => {
                errObj[key] = error[key];
            });
        }
        this.setState({error: errObj});
    }

    onSignOut() {
        this.props.signOut(this.onSuccess, this.onError)
    }

    onSuccess() {
         Actions.replace("Auth")
    }

    onError(error) {
         Alert.alert('Oops!', error.message);
    }

    onSave() {
        console.log("On Save action");
        this.props.saveMovie({currentUser: this.props.authState.user, movie: this.props.homeState.data}, this.onSuccessSave, this.onErrorSave)
    }

    onSuccessSave() {
        console.log("Successfully saved movie: ", this.props.homeState.data);
         // Actions.replace("Auth")
    }

    onErrorSave(error) {
        console.log("Error when trying to save movie");
         // Alert.alert('Oops!', error.message);
    }

    render() {
        let homeState = this.props.homeState;
        console.log('homeState-- ', homeState);
        let title = homeState.data && homeState.data.title ? "Details of " + homeState.data.title : "Without details";
        console.log('title-- ', title);
        let posterPath = homeState.data && homeState.data.poster_path ? homeState.data.poster_path : '';
        let posterFullDir = homeState.configData && homeState.configData.images ? homeState.configData.images.base_url + homeState.configData.images.poster_sizes[4] + posterPath : '';
        console.log('poster-- ', posterFullDir);
        // console.log('defaultImage-- ', defaultImage);
        return (
            <ScrollView style={styles.container}>
                <SearchForm fields={fields}
                    showLabel={false}
                    onSearchMovie={this.onSearchMovie}
                    onSearchDetails={this.onSearchDetails}
                    buttonTitle={"SEARCH"}
                    error={this.state.error}
                    films={this.state.films}
                />
                <Card
                    wrapperStyle={{justifyContent: 'center'}}
                    title={title}
                >
                    <Image
                        style={{width: 250, height: 375}}
                        source={{ uri: posterFullDir}}
                    />
                    {!!homeState.data && !!homeState.data.overview && (<View><Text> {homeState.data.overview} </Text></View>)}
                    {!!homeState.data && !!homeState.data.homepage && (<View><Text> {homeState.data.homepage}</Text></View>)}
                    {!!homeState.data && !!homeState.data.release_date && (<View><Text> {homeState.data.release_date}</Text></View>)}
                    {!!homeState.data && !!homeState.data.tagline && (<View><Text>{homeState.data.tagline}</Text></View>)}
                </Card>
                <Button
                    raised
                    borderRadius={4}
                    text={'SAVE'}
                    containerViewStyle={[styles.containerView]}
                    buttonStyle={[styles.buttonSaveContainer]}
                    textStyle={styles.buttonText}
                    onPress={this.onSave}
                />
                <Button
                    raised
                    borderRadius={4}
                    text={'LOG OUT'}
                    containerViewStyle={[styles.containerView]}
                    buttonStyle={[styles.buttonLogoutContainer]}
                    textStyle={styles.buttonText}
                    onPress={this.onSignOut}
                />
            </ScrollView>
        );
    }
}

const mapStateToProps = (state, ownProps = {}) => ({
    authState: state.authReducer,
    homeState: state.homeReducer
});

const mapDispatchToProps = {
    signOut,
    fetchMovie,
    fetchMovieDetails,
    getConfig,
    saveMovie
}

// export default connect(mapStateToProps, { signOut, fetchMovie, fetchMovieDetails, getConfig })(Home);
export default connect(mapStateToProps, mapDispatchToProps)(Home);
