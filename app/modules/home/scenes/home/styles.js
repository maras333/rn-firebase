import { StyleSheet } from 'react-native';
import { theme } from "../../index"
const { padding, color, fontSize, fontFamily, windowWidth, normalize } = theme;

const resizeMode = 'contain';

const styles = StyleSheet.create({
    container:{
        flex:1,
    },

    bottomContainer:{
        backgroundColor:"white",
        paddingVertical: padding * 3,
        shadowColor: "#000000",
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 0
        }
    },

    buttonLogoutContainer:{
        position: 'relative',
        justifyContent:"center",
        alignItems:"center",
    },

    buttonSaveContainer:{
        position: 'relative',
        justifyContent:"center",
        alignItems:"center"
    },

    inputContainer: {
      width: windowWidth - 40,
      height: normalize(65),
      fontSize: fontSize.regular + 2,
      fontWeight: "600",
      borderBottomColor: "#A5A7A9"
  },

    textTitle: {
        fontWeight: 'bold'
    }

});

export default styles;
