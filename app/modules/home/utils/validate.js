export function isEmpty(str) {
  return (!str || 0 === str.length);
}

export function validateSearch(form) {
  let error = {};
  let success = true;

  var keys = Object.keys(form);
  var length = keys.length;

  keys.slice(0, length).map(field => {
      if(field !== "error") {
          var {type, value} = form[field];
          if(isEmpty(value)) {
              error[field] = 'Your ' + field + ' is required';
              success = false;
          }
      }
  });
  return {success, error};
}
