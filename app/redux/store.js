
import { createStore, applyMiddleware, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import reducers from './rootReducer'; //Import the root reducer

const enhancer = composeWithDevTools(applyMiddleware(thunk));

// Connect our store to the reducers
export default createStore(reducers, enhancer);
